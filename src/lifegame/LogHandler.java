/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lifegame;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author Baka
 */
public class LogHandler
{
    private File logFile;
    private PrintWriter writer;
    private String runtimePath;
    
    public LogHandler()
    {
        try
        {
            runtimePath = LogHandler.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()
                    .replace("LifeGame.jar", "");
            runtimePath += "\\LogFile.txt";
            
            logFile = new File(FileSystemView.getFileSystemView().getHomeDirectory().getPath() + "\\LogFile.txt");
            writer = new PrintWriter(new File(runtimePath));     
            
            if (!logFile.exists())
                logFile.createNewFile();
            
            writer.println("*** START OF FILE ***");
        }
        catch (URISyntaxException | IOException exc)
        {
            System.err.println("Unable to create Log file! Please check your runtime path. " + exc.getLocalizedMessage());
        }
    }
    
    public void WriteLineToFile(String text)
    {
        writer.flush();
        writer.println(text);
        writer.flush();
    }
    
    public void WriteStateToFile(double timeStep, double dislocationSum)
    {
        String timeStepStr = String.valueOf(timeStep).replace('.', ',');
        String dislocationSumStr = String.valueOf(dislocationSum).replace("E", "E+").replace('.', ',');
        
        WriteLineToFile(timeStepStr + " " + dislocationSumStr);
    }
    
    public void CloseFile()
    {
        WriteLineToFile("*** END OF DOCUMENT ***");
        
        writer.close();
    }
}
